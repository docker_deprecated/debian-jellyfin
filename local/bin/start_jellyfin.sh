#!/bin/sh

#set -e

RUN_USER_NAME="${RUN_USER_NAME:-root}"
RUN_USER_GROUP="$(id -gn "${RUN_USER_NAME}")"

echo "${RUN_USER_NAME}.${RUN_USER_GROUP}"

su-exec ${RUN_USER_NAME} /usr/bin/jellyfin \
	--datadir /conf.d/jellyfin \
	--cachedir /cache \
	--ffmpeg /usr/lib/jellyfin-ffmpeg/ffmpeg

